#!/bin/bash
file=/tmp/taille.sq
#file=/var/log/mysql/taille.sq
taille=$(ls -la /var/log/mysql/mysql-slow.log | grep / | awk '{ print $5}')
valeur=$((181+$taille))

if [ ! -e "$file" ]; then

    touch "$file" && echo "$valeur" >> $file

fi

ancien="$(cat $file)"
nouveau=$(ls -la /var/log/mysql/mysql-slow.log | grep / | awk '{ print $5}')
hostname=$(hostname)
adresse=$(hostname -I | awk '{print $1}')
log=$(tail -n100 /var/log/mysql/mysql-slow.log)

if [ "$nouveau" -gt "$ancien" ]; then
    mail -a "Content-Type: text/plain; charset=UTF-8" -s 'Alerte : slow query ou redémarrage de mysql' support@axepartner.com << EOF
MYSQL a redémarré OU une slow query a été détectée sur la machine $hostname (adresse IP : $adresse).


Extrait des logs :

$log
EOF

echo $nouveau > $file

fi
