-- MariaDB dump 10.19  Distrib 10.5.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dip
-- ------------------------------------------------------
-- Server version	10.5.12-MariaDB-0+deb11u1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Boucle`
--

DROP TABLE IF EXISTS `Boucle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Boucle` (
  `id_boucle` int(11) NOT NULL AUTO_INCREMENT,
  `id_boucle_type` int(1) NOT NULL,
  `titre` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_boucle`),
  KEY `FK_boucle_id_boucle_type` (`id_boucle_type`),
  CONSTRAINT `FK_boucle_id_boucle_type` FOREIGN KEY (`id_boucle_type`) REFERENCES `Boucle_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Boucle`
--

LOCK TABLES `Boucle` WRITE;
/*!40000 ALTER TABLE `Boucle` DISABLE KEYS */;
/*!40000 ALTER TABLE `Boucle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Boucle_type`
--

DROP TABLE IF EXISTS `Boucle_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Boucle_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Boucle_type`
--

LOCK TABLES `Boucle_type` WRITE;
/*!40000 ALTER TABLE `Boucle_type` DISABLE KEYS */;
INSERT INTO `Boucle_type` VALUES (1,'Média'),(2,'Pied d\'écran');
/*!40000 ALTER TABLE `Boucle_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Corbeille_medias`
--

DROP TABLE IF EXISTS `Corbeille_medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Corbeille_medias` (
  `id_corbeille_medias` int(11) NOT NULL AUTO_INCREMENT,
  `id_media_axe` int(11) NOT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `full_screen` int(11) DEFAULT NULL,
  `texte` longtext DEFAULT NULL,
  PRIMARY KEY (`id_corbeille_medias`),
  KEY `utilisateur_idx` (`id_utilisateur`),
  CONSTRAINT `utilisateur_corbeille` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Corbeille_medias`
--

LOCK TABLES `Corbeille_medias` WRITE;
/*!40000 ALTER TABLE `Corbeille_medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `Corbeille_medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Diffusion`
--

DROP TABLE IF EXISTS `Diffusion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Diffusion` (
  `id_diffusion` int(11) NOT NULL AUTO_INCREMENT,
  `id_boucle` int(11) DEFAULT NULL,
  `id_player` int(11) DEFAULT NULL,
  `ordre` int(11) NOT NULL,
  PRIMARY KEY (`id_diffusion`),
  KEY `boucle_idx` (`id_boucle`),
  KEY `player_idx` (`id_player`),
  CONSTRAINT `FK_DBC9C3F82B4BCE28` FOREIGN KEY (`id_boucle`) REFERENCES `Boucle` (`id_boucle`),
  CONSTRAINT `FK_DBC9C3F8BE2ECF88` FOREIGN KEY (`id_player`) REFERENCES `Player` (`id_player`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Diffusion`
--

LOCK TABLES `Diffusion` WRITE;
/*!40000 ALTER TABLE `Diffusion` DISABLE KEYS */;
/*!40000 ALTER TABLE `Diffusion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Droit_utilisateur`
--

DROP TABLE IF EXISTS `Droit_utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Droit_utilisateur` (
  `id_droit_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `id_player` int(11) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_droit_utilisateur`),
  KEY `player_idx` (`id_player`),
  KEY `utilisateur_idx` (`id_utilisateur`),
  CONSTRAINT `FK_C963D2DB50EAE44` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateur` (`id_utilisateur`),
  CONSTRAINT `FK_C963D2DBBE2ECF88` FOREIGN KEY (`id_player`) REFERENCES `Player` (`id_player`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Droit_utilisateur`
--

LOCK TABLES `Droit_utilisateur` WRITE;
/*!40000 ALTER TABLE `Droit_utilisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `Droit_utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Etablissement`
--

DROP TABLE IF EXISTS `Etablissement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Etablissement` (
  `id_etablissement` int(11) NOT NULL AUTO_INCREMENT,
  `id_groupe` int(11) DEFAULT NULL,
  `nom` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdp` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_etablissement`),
  KEY `groupe_idx` (`id_groupe`),
  CONSTRAINT `FK_938312D9228E39CC` FOREIGN KEY (`id_groupe`) REFERENCES `Groupe` (`id_groupe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Etablissement`
--

LOCK TABLES `Etablissement` WRITE;
/*!40000 ALTER TABLE `Etablissement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Etablissement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groupe`
--

DROP TABLE IF EXISTS `Groupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groupe` (
  `id_groupe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdp` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port_ftp` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_ftp` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pass_ftp` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cle_ftp` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_groupe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groupe`
--

LOCK TABLES `Groupe` WRITE;
/*!40000 ALTER TABLE `Groupe` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groupe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Medias`
--

DROP TABLE IF EXISTS `Medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Medias` (
  `id_medias` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_media_type` int(1) NOT NULL,
  `id_media_axe` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_fichier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duree` int(11) DEFAULT 0,
  `creation` datetime DEFAULT NULL,
  `modification` datetime DEFAULT NULL,
  `plein_ecran` tinyint(1) DEFAULT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_supprimer` tinyint(1) NOT NULL DEFAULT 0,
  `canvas_json` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_medias`),
  KEY `utilisateur_idx` (`id_utilisateur`),
  KEY `FK_media_id_media_type` (`id_media_type`),
  CONSTRAINT `FK_157EAAB750EAE44` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateur` (`id_utilisateur`),
  CONSTRAINT `FK_media_id_media_type` FOREIGN KEY (`id_media_type`) REFERENCES `Medias_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Medias`
--

LOCK TABLES `Medias` WRITE;
/*!40000 ALTER TABLE `Medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `Medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Medias_boucle`
--

DROP TABLE IF EXISTS `Medias_boucle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Medias_boucle` (
  `id_medias_boucle` int(11) NOT NULL AUTO_INCREMENT,
  `id_boucle` int(11) DEFAULT NULL,
  `id_medias` int(11) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_medias_boucle`),
  KEY `boucle_idx` (`id_boucle`),
  KEY `medias_idx` (`id_medias`),
  CONSTRAINT `FK_883F26E32B4BCE28` FOREIGN KEY (`id_boucle`) REFERENCES `Boucle` (`id_boucle`),
  CONSTRAINT `FK_883F26E334E51A6C` FOREIGN KEY (`id_medias`) REFERENCES `Medias` (`id_medias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Medias_boucle`
--

LOCK TABLES `Medias_boucle` WRITE;
/*!40000 ALTER TABLE `Medias_boucle` DISABLE KEYS */;
/*!40000 ALTER TABLE `Medias_boucle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Medias_type`
--

DROP TABLE IF EXISTS `Medias_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Medias_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Medias_type`
--

LOCK TABLES `Medias_type` WRITE;
/*!40000 ALTER TABLE `Medias_type` DISABLE KEYS */;
INSERT INTO `Medias_type` VALUES (1,'Messages pré-construits'),(2,'Messages personnalisables'),(3,'Messages pieds d\'écran'),(4,'Vidéos');
/*!40000 ALTER TABLE `Medias_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Player`
--

DROP TABLE IF EXISTS `Player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Player` (
  `id_player` int(11) NOT NULL AUTO_INCREMENT,
  `id_etablissement` int(11) DEFAULT NULL,
  `nom` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port_vnc` int(11) NOT NULL DEFAULT 5901,
  `pass_vnc` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'diffmed',
  `emplacement` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_player`),
  KEY `etablissement_idx` (`id_etablissement`),
  CONSTRAINT `FK_9FB57F539ED58849` FOREIGN KEY (`id_etablissement`) REFERENCES `Etablissement` (`id_etablissement`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Player`
--

LOCK TABLES `Player` WRITE;
/*!40000 ALTER TABLE `Player` DISABLE KEYS */;
/*!40000 ALTER TABLE `Player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Session`
--

DROP TABLE IF EXISTS `Session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Session`
--

LOCK TABLES `Session` WRITE;
/*!40000 ALTER TABLE `Session` DISABLE KEYS */;
/*!40000 ALTER TABLE `Session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Utilisateur`
--

DROP TABLE IF EXISTS `Utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pass` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Utilisateur`
--

LOCK TABLES `Utilisateur` WRITE;
/*!40000 ALTER TABLE `Utilisateur` DISABLE KEYS */;
INSERT INTO `Utilisateur` VALUES (1,'admin','admin','admin','df2c47a972dcae68ee2d459af27ddb34',0,'[\"ROLE_USER\",\"ROLE_ADMIN\"]');
/*!40000 ALTER TABLE `Utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('eb7679054232d2c1c82a8e4af4abbafb76d6d7b0','192.168.20.187',1564156351,'__ci_last_regenerate|i:1564156340;id_utilisateur|s:88:\"kTXjvVGErTGIn186chBnya9tXlBRI9qdS5z1oMDCfTbu4sbePdYyYMKUJiK+abSNLP2Qh84ti0ShLIfZ9qDiRA==\";nom|s:88:\"s4eF6FNfmpZsGX+c+eKkG9XK6p0d4kJh8lGMh2cgxi2AerkmpeATgRxq8iQ8SnXNENJMt2C1OHBIRpPWRez/aA==\";prenom|s:88:\"ztmDGIQLKJwXzpjiyYHMi+0gHGFOuHlE+skJyznpdRgvfPGv8YTgov0n1DPHsO/M0y7mKzw9sH9wGwx3X+OPsg==\";logged|s:88:\"RaY/4jV19R1Q7YoVPb/luFybIWCxZkfjecJwZSnWkcR+DC4xgXIqfXTfR4xYRqJ/D33QH9lsQH2txkWXAqWrig==\";type_user|s:88:\"ZBgIKTFygE+cDu/gJQS+bis0vQTciOQPNrHPhZs+Sp1NwoLQ5Y/XiM+7p/SA24VIDVCfvMXFkVqrySB/L6OJMw==\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190828152121','2019-08-29 08:39:11'),('20190905155353','2019-09-06 08:57:32'),('20190910073232','2019-09-10 07:46:37'),('20191001123303','2019-10-01 14:01:41'),('20191030144623','2019-10-30 14:53:41'),('20210916093118','2021-11-19 16:02:24'),('20211123143903','2021-11-23 14:41:42'),('20211124152534','2021-11-25 09:18:08'),('20211126103007','2021-11-26 10:37:43'),('20211129101358','2021-11-29 10:37:07');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:00:06
