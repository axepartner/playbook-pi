$Conf{BackupPCNightlyPeriod} = 1;
$Conf{BlackoutPeriods} = [
  {
    'hourBegin' => 7,
    'hourEnd' => 20,
    'weekDays' => [
      1,
      2,
      3,
      4,
      5
    ]
  }
];
$Conf{DumpPreUserCmd} = '$sshPath -q -x -l root $host /root/scripts/backup_bdd.sh';
$Conf{FullAgeMax} = 15;
$Conf{RsyncShareName} = [
  '/etc',
  '/home/autossh/.ssh',
  '/home/diffmed/.ssh',
  '/root/.ssh',
  '/root/backup_bdd',
  '/var/spool/cron/crontabs',
  '/var/www/dfa/shared',
  '/var/www/dip/shared',
  '/var/www/interop/shared',
  '/var/www/envoi-msg/shared'
];
$Conf{XferMethod} = 'rsync';
