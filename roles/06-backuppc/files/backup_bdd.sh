#!/bin/bash

# Fonction pour ajouter un horodatage à chaque message de log
log_with_timestamp() {
    echo "$(date +%Y-%m-%dT%H:%M:%S) - $1" >> /root/backup_bdd/backup_bdd.log
}

# date du jour
backupdate=$(date +%Y-%m-%d)
current_time=$(date +%Y-%m-%d_%H-%M-%S)

# répertoire de backup
dirbackup=/root/backup_bdd/backup-$backupdate

# création du répertoire de backup
/bin/mkdir -p $dirbackup

# Initialisation du fichier de log
log_with_timestamp "Backup started"
log_with_timestamp "Backup directory: $dirbackup"

# on se place dans le repertoire ou l'on veut sauvegarder les bases
cd $dirbackup

# Récupère toutes les bases de données
databases=$(mysql -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema)")
if [ $? -ne 0 ]; then
    log_with_timestamp "Error fetching databases"
    exit 1
fi

# parcours les bases
for i in $databases; do
    start_time=$(date +%Y-%m-%d_%H-%M-%S)
    log_with_timestamp "Starting backup for database $i"

    # sauvegarde des bases de donnees en fichiers .sql
    mysqldump --skip-lock-tables ${i} > ${i}_$current_time.sql
    if [ $? -ne 0 ]; then
        log_with_timestamp "Error dumping database $i"
        continue
    fi

    # compression des exports en tar.bz2 (le meilleur taux de compression)
    tar jcf ${i}_$current_time.sql.tar.bz2 ${i}_$current_time.sql
    if [ $? -ne 0 ]; then
        log_with_timestamp "Error compressing dump file for database $i"
        rm ${i}_$current_time.sql
        continue
    fi

    # suppression des exports non compresses
    rm ${i}_$current_time.sql
    log_with_timestamp "Backup and compression successful for database $i"
done

# supprime les sauvegardes vieilles de plus de 6 jours
cd /root/backup_bdd/
find . -maxdepth 1 -type d -name "backup-*" | while read dir; do
    dir_date=$(basename $dir | cut -d'-' -f2-)
    dir_timestamp=$(date -d $dir_date +%s)
    cutoff_timestamp=$(date -d "$backupdate -6 days" +%s)

    if [ $dir_timestamp -lt $cutoff_timestamp ]; then
        log_with_timestamp "Deleting old backup directory $dir"
        rm -rf $dir
    fi
done

end_time=$(date +%Y-%m-%d_%H-%M-%S)
log_with_timestamp "Backup completed"
